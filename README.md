# Login Page

**PPW Tugas 9 Project**

## Author

* Hans Michael Nabasa Pasaribu - 1806235662

## Herokuapp Link

[Login Practise](https://loginpractise.herokuapp.com) 

## Project Details

Web ini berfungsi untuk menambahkan login page sebelum memunculkan isi web yang seharusnya menggunakan authentication, session, dan cookie.

## Pipelines Status

[![pipeline status](https://gitlab.com/hansmichael.hm/tugas-9/badges/master/pipeline.svg)](https://gitlab.com/hansmichael.hm/tugas-9/commits/master)

## Coverage Status
[![coverage report](https://gitlab.com/hansmichael.hm/tugas-9/badges/master/coverage.svg)](https://gitlab.com/hansmichael.hm/tugas-9/commits/master)